#!/bin/bash

#
# Version 00    : Initial Version. ( Shrijan Chipalu )
# Version 01    : Updated script with if else condition to output with failed and successful status.
#                               : Update by Shrijan Chipalu

#BKP_CLI=dc5b00
#MYSQL_USER=root
#MYSQL_PASS=**********
#MYSQL_BKP_DIR=/backup/mysql
#MYSQL_BKP_FILE=all_databases.sql

#echo "============================================================="
#echo "ssh $BKP_CLI \"mysqldump --user=$MYSQL_USER --password=********** --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql\""
#ssh $BKP_CLI "mysqldump --user=$MYSQL_USER --password=$MYSQL_PASS --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql"
#if [ $? -ne 0 ]
#then
#       echo "Backup failed for mysql"
#else
#       echo "Backup completed for mysql"
#fi




BKP_CLI=dc5b3c
MYSQL_USER=root
MYSQL_PASS=****************
MYSQL_BKP_DIR=/backup/mysql
#MYSQL_BKP_FILE=dev_all_databases.sql

echo "============================================================="
echo "ssh $BKP_CLI \"/var/lib/mysql-8.0/bin/mysqldump --user=$MYSQL_USER --password=********** --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql\""
ssh $BKP_CLI "/var/lib/mysql-8.0/bin/mysqldump --user=$MYSQL_USER --password=$MYSQL_PASS --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql"
if [ $? -ne 0 ]
then
        echo "Backup failed for dev cm mysql"
else
        echo "Backup completed for dev cm mysql"
fi

#BKP_CLI=dc5b2i
#MYSQL_USER=root
#MYSQL_PASS=********
#MYSQL_BKP_DIR=/backup/mysql
#MYSQL_BKP_FILE=dev_all_databases.sql

#echo "============================================================="
#echo "ssh $BKP_CLI \"mysqldump --user=$MYSQL_USER --password=********** --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql\""
#ssh $BKP_CLI "mysqldump --user=$MYSQL_USER --password=$MYSQL_PASS --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql"
#if [ $? -ne 0 ]
#then
#        echo "Backup failed for reporting cm mysql"
#else
#        echo "Backup completed for reporting cm mysql"
#fi

BKP_CLI=dc5b1i
MYSQL_USER=root
MYSQL_PASS=****************
MYSQL_BKP_DIR=/backup/mysql
#MYSQL_BKP_FILE=dev_all_databases.sql

echo "============================================================="
echo "ssh $BKP_CLI \"/var/lib/mysql-8.0/bin/mysqldump --user=$MYSQL_USER --password=********** --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql\""
ssh $BKP_CLI "/var/lib/mysql-8.0/bin/mysqldump --user=$MYSQL_USER --password=$MYSQL_PASS --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql"
if [ $? -ne 0 ]
then
        echo "Backup failed for lab cm mysql"
else
        echo "Backup completed for lab cm mysql"
fi


#BKP_CLI=dc5b40
#MYSQL_USER=root
#MYSQL_PASS=****************
#MYSQL_BKP_DIR=/backup/mysql
#MYSQL_BKP_FILE=dev_all_databases.sql

#echo "============================================================="
#echo "ssh $BKP_CLI \"/var/lib/mysql-8.0/bin/mysqldump --user=$MYSQL_USER --password=********** --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql\""
#ssh $BKP_CLI "/var/lib/mysql-8.0/bin/mysqldump --user=$MYSQL_USER --password=$MYSQL_PASS --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql"
#if [ $? -ne 0 ]
#then
#        echo "Backup failed for prod2 cm mysql"
#else
#        echo "Backup completed for prod2 cm mysql"
#fi


#backup Kyvos db
BKP_CLI=dc5b3n
MYSQL_BKP_DIR=/backup/mysql

echo "============================================================="
echo "ssh $BKP_CLI \"/var/lib/mysql-8.0/bin/mysqldump --user=postgres --password=********** --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI\""
ssh $BKP_CLI "cd /opt/install/app/kyvos/postgres/bin/; ./pg_dump -U postgres -h localhost -p 45421 -d delverepo > /backup/mysql/kyvos_db_backup"
if [ $? -ne 0 ]
then
        echo "Backup failed for kyvos postgres"
else
        echo "Backup completed for kyvos postgres"
fi

BKP_CLI=dc5b5l
MYSQL_USER=root
MYSQL_PASS=****************
MYSQL_BKP_DIR=/backup/mysql
#MYSQL_BKP_FILE=dev_all_databases.sql

echo "============================================================="
echo "ssh $BKP_CLI \"/var/lib/mysql-8.0/bin/mysqldump --user=$MYSQL_USER --password=********** --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql\""
ssh $BKP_CLI "/var/lib/mysql-8.0/bin/mysqldump --user=$MYSQL_USER --password=$MYSQL_PASS --all-databases --events > $MYSQL_BKP_DIR/all_databases.$BKP_CLI.sql"
if [ $? -ne 0 ]
then
        echo "Backup failed for prod2 cm mysql"
else
        echo "Backup completed for prod2 cm mysql"
fi

